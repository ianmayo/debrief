package MWC.GenericData;

/** marker interface for objects that should not be painted in a color
 * 
 * @author ian
 *
 */
public interface NonColoredWatchable
{

}
