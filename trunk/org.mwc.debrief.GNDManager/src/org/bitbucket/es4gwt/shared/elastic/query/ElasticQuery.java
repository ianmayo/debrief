package org.bitbucket.es4gwt.shared.elastic.query;

import org.bitbucket.es4gwt.shared.elastic.ElasticRequestElement;

/**
 * @author Mikael Couzic
 */
public interface ElasticQuery extends ElasticRequestElement {

}
