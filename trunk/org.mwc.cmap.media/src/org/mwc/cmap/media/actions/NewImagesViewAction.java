package org.mwc.cmap.media.actions;

import org.mwc.cmap.media.views.ImagesView;

public class NewImagesViewAction extends BasePlanetmayoAction {

	public NewImagesViewAction() {
		super(ImagesView.ID);
	}
}
