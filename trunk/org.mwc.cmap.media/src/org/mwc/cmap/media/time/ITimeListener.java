package org.mwc.cmap.media.time;

public interface ITimeListener {
	
	void newTime(Object src, long millis);
}
