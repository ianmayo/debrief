package org.mwc.cmap.media.xuggle;

public class PlayerAdapter implements PlayerListener {

	@Override
	public void onPlaying(XugglePlayer player, long milli) {
		
	}

	@Override
	public void onPlay(XugglePlayer player) {
		
	}

	@Override
	public void onStop(XugglePlayer player) {
		
	}

	@Override
	public void onPause(XugglePlayer player) {
		
	}

	@Override
	public void onSeek(XugglePlayer player, long milli) {
		
	}

	@Override
	public void onVideoOpened(XugglePlayer player, String fileName) {
		
	}
}
