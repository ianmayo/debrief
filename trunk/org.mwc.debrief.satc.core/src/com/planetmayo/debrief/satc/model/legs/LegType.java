package com.planetmayo.debrief.satc.model.legs;

public enum LegType
{
		STRAIGHT, ALTERING
}
