package com.planetmayo.debrief.satc.model.contributions;

public enum ContributionDataType
{
	FORECAST, ANALYSIS, MEASUREMENT
}
